Para usar la extensión debe situarse en la pagina del carrito de mercado libre y ejecutar la extensión

![alt text](https://i.ibb.co/R27tydC/Captura-de-Pantalla-2021-03-18-a-la-s-11-24-28-a-m.png "Extensión screen capture")

Al presionar el boton Extract generara un CSV con los articulos del carrito.

![alt text](https://i.ibb.co/HKw76MZ/Captura-de-Pantalla-2021-03-18-a-la-s-11-27-35-a-m.png "Descarga screen capture")

![alt text](https://i.ibb.co/gt9qMbc/Captura-de-Pantalla-2021-03-18-a-la-s-11-30-07-a-m.png "Descarga screen capture")
