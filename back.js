var itemElements = document.querySelectorAll("article.item");
var items = [];
if(itemElements){
    itemElements.forEach((itemElemtn)=>{
        items.push({
            title: itemElemtn.querySelector(".item__title").innerText,
            subtitle: itemElemtn.querySelector(".item__subtitle").innerText,
            quantity: itemElemtn.querySelector("input[name='quantity']").value,
            price: itemElemtn.querySelector("[itemprop='price']").getAttribute("content"),
            img: itemElemtn.querySelector("img").getAttribute("src")
        })
    });
}
arrayObjToCsv(items);
function arrayObjToCsv(ar) {
	if(window.Blob && (window.URL || window.webkitURL)){
		var contenido = "",
			d = new Date(),
			blob,
			reader,
			save,
			clicEvent;
		for (var i = 0; i < ar.length; i++) {
			if (i == 0)
				contenido += Object.keys(ar[i]).join(";") + "\n";
			contenido += Object.keys(ar[i]).map(function(key){
							return ar[i][key];
						}).join(";") + "\n";
		}
		blob =  new Blob(["\ufeff", contenido], {type: 'text/csv'});
		var reader = new FileReader();
		reader.onload = function (event) {
			//escuchamos su evento load y creamos un enlace en dom
			save = document.createElement('a');
			save.href = event.target.result;
			save.target = '_blank';
			//aquí le damos nombre al archivo
			save.download = "ML_scraping_"+ d.getDate() + "_" + (d.getMonth()+1) + "_" + d.getFullYear() +".csv";
			try {
				//creamos un evento click
				clicEvent = new MouseEvent('click', {
					'view': window,
					'bubbles': true,
					'cancelable': true
				});
			} catch (e) {
				//si llega aquí es que probablemente implemente la forma antigua de crear un enlace
				clicEvent = document.createEvent("MouseEvent");
				clicEvent.initEvent('click', true, true);
			}
			//disparamos el evento
			save.dispatchEvent(clicEvent);
			(window.URL || window.webkitURL).revokeObjectURL(save.href);
		}
		reader.readAsDataURL(blob);
	}else {
		alert("Su navegador no permite esta acción");
	}
};
